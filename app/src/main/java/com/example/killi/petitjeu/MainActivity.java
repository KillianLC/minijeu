package com.example.killi.petitjeu;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumber = null;
    private TextView lbResult;
    private Button btnComparer;
    private ProgressBar pgbScore;
    private TextView lblOutput;

    private int searchedValue;
    private int score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumber = findViewById(R.id.edNumber);
        lbResult = findViewById(R.id.tvResult);
        pgbScore = findViewById(R.id.pgbScore);
        lblOutput = findViewById(R.id.lbOutput);
        btnComparer = findViewById(R.id.btnCompare);

        btnComparer.setOnClickListener(btnCompareListner);
        Init();
    }
    private void Init(){
        score = 0;
        searchedValue = 1+(int)(Math.random()*100);
        Log.i("DEBUG","Valeur cherchée : "+searchedValue);

        txtNumber.setText("");
        pgbScore.setProgress(score);
        lbResult.setText("");
        lblOutput.setText("");

        txtNumber.requestFocus();
    }
    private View.OnClickListener btnCompareListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i("DEBUG", "Bouton Cliqué");

            String strNumber = txtNumber.getText().toString();
            if(strNumber.equals(""))return;

            lblOutput.append(strNumber+"\r\n");
            pgbScore.incrementProgressBy(1);
            score++;

            int enteredValue = Integer.parseInt(strNumber);
            if(enteredValue == searchedValue){
                congratulation();
            }else if(enteredValue< searchedValue){
                lbResult.setText(getString(R.string.strGreater));
            }else{
                lbResult.setText(getString(R.string.strLower));
            }
            txtNumber.setText("");
            txtNumber.requestFocus();
        }
    };
    private void congratulation(){
        lbResult.setText(getString(R.string.strCongratulation));

        AlertDialog retryAlert = new AlertDialog.Builder(this).create();
        retryAlert.setTitle(R.string.app_name);
        retryAlert.setMessage(getString(R.string.strMsg, score));

        retryAlert.setButton(AlertDialog.BUTTON_POSITIVE,getString(R.string.strYes), new AlertDialog.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                Init();
            }
        });
        retryAlert.setButton(AlertDialog.BUTTON_NEGATIVE,getString(R.string.strNo), new AlertDialog.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                finish();
            }
        });

        retryAlert.show();
    }
}
